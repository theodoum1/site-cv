import React from 'react';
import './App.css';
import Header_cv from './features/Header';

function App() {
  return (
    <div className="App">
      <Header_cv />
    </div>
  );
}

export default App;
