import React, { Component } from 'react';
import '../App.css';

import { Grommet, Header } from 'grommet';
import { grommet } from 'grommet/themes';
import Content from './Content';
import Contact from './Contact';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

import Alsatis_exp from './Experiences/Alsatis';
import Coved_exp from './Experiences/Coved';
import Laposte_exp from './Experiences/Laposte';

import { Button } from 'grommet';

class Header_cv extends Component {
    render() {
        return (
            <Grommet theme={grommet} >
                <Router>
                    <Header className="header_top" background="light-4" pad="medium" height="xsmall">
                        <Button primary label="Accueil" href="/" />
                        <h2 className="title_header">DOUMENG Théo</h2>
                        <Button primary label="Contact" href="/contact" />
                    </Header>
                    <Switch>
                        <Route path="/contact">
                            <Contact_me />
                        </Route>
                        <Route path="/laposte">
                            <Laposte />
                        </Route>
                        <Route path="/coved">
                            <Coved />
                        </Route>
                        <Route path="/alsatis">
                            <Alsatis />
                        </Route>
                        <Route path="/">
                            <Home />
                        </Route>
                    </Switch>
                </Router>
            </Grommet>
        );
    }
}

function Laposte() {
    return (
        <Laposte_exp />
    );
}

function Coved() {
    return (
        <Coved_exp />
    );
}

function Alsatis() {
    return (
        <Alsatis_exp />
    );
}

function Contact_me() {
    return (
        <Contact />
    );
}

function Home() {
    return (
        <Content />
    );
}

export default Header_cv;