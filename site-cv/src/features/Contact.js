import React, { Component } from 'react';
import '../App.css';

class Contact extends Component {
    render() {
        return (
            <div className="content">
                <h1>Me contactez</h1>
                <a href="mailto:theodoum1@gmail.com">theodoum1@gmail.com</a>
                <br></br>
                <a href="tel:+33643439443">06 43 43 94 43</a>
            </div>
        )
    }
}

export default Contact;