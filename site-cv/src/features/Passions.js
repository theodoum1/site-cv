import React from 'react';
import { Box, Grommet, grommet, Tab, Tabs } from 'grommet';
import { deepMerge } from 'grommet/utils';
import { Text } from 'grommet';

const myTheme = deepMerge(grommet, {
    tabs: {
      header: {
        border: {
          side: 'bottom',
          color: 'blue',
          size: 'small',
        },
      },
    },
    tab: {
      border: {
        side: 'bottom',
        color: 'dark-4',
      },
      pad: 'small',
      margin: {
        vertical: '-2px',
        horizontal: 'none',
      },
    },
  });
  
  const Passions = () => (
    <Grommet theme={myTheme} full className="div_passions">
      <Tabs justify="start" alignControls="start">
        <Tab title="Le sport" className="sous-menu_passions">
          <Box fill pad="large" align="center" className="text_passion">
            <Text>Je suis passioné de sport. Depuis que je suis petit j'ai pratiqué différents sports en compétition. Actuellement je suis 
                dans un club de volley.
            </Text>
          </Box>
        </Tab>
        <Tab title="Le jeux vidéo" className="sous-menu_passions">
          <Box fill pad="large" align="center" className="text_passion">
          <Text>J'ai une autre passion qui est le jeu vidéo. J'y joue assez régulièrement. J'aime jouer avec mes amis ça rend les 
              parties plus drôle.</Text>
          </Box>
        </Tab>
        <Tab title="L'informatique" className="sous-menu_passions">
          <Box fill pad="large" align="center" className="text_passion">
          <Text>J'ai toujours été attiré par les technologiese et plus particulièrement, l'informatique. J'ai toujours voulu savoir ce qu'il
                se passais de l'autre côté de la machine.
          </Text>
          </Box>
        </Tab>
      </Tabs>
    </Grommet>
  );

  export default Passions;