import React, { Component } from 'react';

import { Grommet, Image, Text } from 'grommet';

class Alsatis_exp extends Component {
    render() {
        return (
            <Grommet>
                <div className="content">
                    <Image src="https://alsatis.com/wp-content/uploads/2020/08/Alsatis_logo_Telecom.png" />
                    <br></br>
                    <Text>J'ai effectué un stage chez Alsatis dont mon objectif était de développer une assistance en ligne sur le site de l'entreprise.</Text>
                    <Text>J'ai réaliser ce projet avec succès, suite à cela, l'entreprise à décidé de me prendre en alternance pour faire la refonte du site.</Text>
                    <Text>Aujourd'hui je suis toujours en alternance dans cette entreprise.</Text>
                </div>
            </Grommet>
        );
    }
}

export default Alsatis_exp;