import React, { Component } from 'react';

import { Grommet, Image, Text } from 'grommet';

class Laposte_exp extends Component {
    render() {
        return (
            <Grommet>
                <div className="content">
                    <Image src="https://www.laposte.fr/_ui/eboutique/images/fusion/logolp_part.svg" />
                    <br></br>
                    <Text>J'ai été embauché en tant que facteur à Laposte. Ma mission consistait à livré le couriel et les colis.</Text>
                    <Text>J'ai exécuté 4 semaines de travail dans cette entreprise.</Text>
                </div>
            </Grommet>
        );
    }
}

export default Laposte_exp;