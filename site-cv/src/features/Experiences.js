import React, { Component } from 'react';
import '../App.css';

import { Grommet, Anchor, List } from 'grommet';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const customTheme = {
  carousel: {
    animation: {
      duration: 400,
    },
    icons: {
      color: 'blue',
    },
    disabled: {
      icons: {
        color: 'grey',
      },
    },
  },
};

const Experiences = (props) => {
  return (
    <Grommet theme={customTheme}>
      <Anchor href={"/" + props.title} label={props.title.toUpperCase()} />
    </Grommet>
  );
}

export default Experiences;