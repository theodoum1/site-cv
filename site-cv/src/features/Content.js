import React, { Component } from 'react';
import '../App.css';

import Experiences from './Experiences';
import Presentation from './Presentation';
import Passions from './Passions';
import Skills from './Skills';

import { List } from 'grommet';

class Content extends Component {
    render() {
        return (
            <div className="content">
                <h1 className="ecart-top">Présentation</h1>
                <Presentation />
                <h1 className="ecart-top">Mes expériences</h1>
                <List
                primaryKey="name"
                data={[
                    { name: <Experiences title="alsatis" /> },
                    { name: <Experiences title="laposte" /> },
                    { name: <Experiences title="coved" /> },
                ]}
                />
                <h1 className="ecart-top">Mes passions</h1>
                <Passions />
                <h1 className="ecart-top">Mes compétences</h1>
                <Skills />
            </div>
        )
    }
}

export default Content;