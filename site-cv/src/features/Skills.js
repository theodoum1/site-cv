import React, { Component } from 'react';

import { Grommet, Meter, Text } from 'grommet';

class Skills extends Component {
    render() {
        return (
            <Grommet className="content_skills">
                <Grommet className="content_text_skills">
                    <Text>Frontend : </Text>

                    <br></br>
                    <Text>Backend : </Text>
                    <br></br>
                    <Text>BDD : </Text>
                    <br></br>
                    <Text>Management : </Text>
                </Grommet>
                <Grommet className="content_text_skills">
                    <Meter
                        values={[{
                            color: '#7D4CDB',
                            value: 70,
                            label: 'sixty',
                            onClick: () => { }
                        }]}
                        aria-label="meter"
                    />
                    <br></br>
                    <Meter
                        values={[{
                            color: '#7D4CDB',
                            value: 70,
                            label: 'sixty',
                            onClick: () => { }
                        }]}
                        aria-label="meter"
                    />
                    <br></br>
                    <Meter
                        values={[{
                            color: '#7D4CDB',
                            value: 80,
                            label: 'sixty',
                            onClick: () => { }
                        }]}
                        aria-label="meter"
                    />
                    <br></br>
                    <Meter
                        values={[{
                            color: '#7D4CDB',
                            value: 60,
                            label: 'sixty',
                            onClick: () => { }
                        }]}
                        aria-label="meter"
                    />
                </Grommet>
            </Grommet>
        );
    }
}

export default Skills;